<head>
  <link href="{{url('css/quizzes.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">
  <script src="//cdn.ckeditor.com/4.5.6/full/ckeditor.js"></script>
</head>
<div class="modal fade" id="createLessonModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <i class="fa fa-flash"></i>
          Add Lesson
        </h4>
      </div>
        <div class="modal-body">
           {!!Form::open(['url' => 'courses/'.$course->id.'/add-lesson'])!!}
           <div class="form-group">
               {!! Form::label('description','Introduce lesson') !!}
               {!! Form::textarea('description',null,['class' => 'form-control', 'autocomplete' => 'off']) !!}
           </div>
              <div class="form-group">
                  {!! Form::hidden('id',Auth::user()->id) !!}
              </div>
           <div class="form-group">
               {!! Form::hidden('course_id',$course->id) !!}
           </div>
              <div class="form-group">
                  {!! Form::submit('Add Lesson',['class' => 'btn btn-primary form-control']) !!}
              </div>
              {!! Form::close() !!}
        <script>
            CKEDITOR.replace( 'description' );
        </script>
      </div>
    </div>
  </div>
</div>
