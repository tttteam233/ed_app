@extends('layouts.application')
@section('styles')
  <link href="{{url('css/edit_lesson.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">
  <script src="//cdn.ckeditor.com/4.5.6/full/ckeditor.js"></script>
@stop
@section('content')

{!!Form::open(['url' => 'courses/{id}/{lessNo}'],['id'=>'form1'])!!}
<div class="row">
  <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
  <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 lesson-edit">
    <div class="form-group">
        {!! Form::hidden('id',Auth::user()->id) !!}
    </div>
    <div class="form-group">
        {!! Form::hidden('course_id',$course_id) !!}
    </div>
    <div class="form-group">
        {!! Form::hidden('lesson_id',$lesson->id) !!}
    </div>
    <div class="form-group">
        {!! Form::textarea('description',$lesson->description,['class' => 'form-control','id'=>'description']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Save',['class' => 'btn btn-primary btn-sm form-control','id'=>'save']) !!}
    </div>
    {!! Form::close() !!}
  </div>
  <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
<script>
  CKEDITOR.replace( 'description' );
</script>
@stop
