@extends('layouts.application')
@section('styles')
  <link href="{{url('css/forum.css')}}" rel="stylesheet">
@stop
@section('content')
@if (count($topics) > 0)
  <div class="table-responsive">
    <table class="table">
      <tr>
        <th colspan="5">
          <h4>Topics</h4>
        </th>
      </tr>
      @foreach ($topics as $topic)
        <tr>
          <td>
            <div class="bullet filled"></div>
          </td>
          <td>
            <a href="/forum/show/{{ $topic->id }}">{{$topic->title}}</a>
            <p>By: <b>{{$topic->owner_name()}}</b></p>
          </td>
          <td>
            <p>{{count($topic->comments)}} comments</p>
          </td>
          <td>
            @if (count($topic->comments) != 0)
              <p>Last: <b>{{str_limit($topic->comments[$topic->comments->count() - 1]->comment,25)}}</b></p>
              <p>By: <b>{!!$topic->comments[$topic->comments->count() - 1]->owner_name()!!}</b> at <b>{!!date("d M Y H:i",strtotime($topic->comments[$topic->comments->count() - 1]->created_at))!!} </b></p>
            @else
              <p>No comments</p>
            @endif
          </td>
          <td>
            <div class="form-group" style="float: left; margin-right: 10px">
              <a href="/forum/show/{{$topic->id}}", class="btn btn-primary btn-xs">View</a>
            </div>
            @if (Auth::user()->isAdmin() || (Auth::user() == $topic->owner()))
              {!! Form::open(['url' => 'forum/delete', 'method' => 'post']) !!}
                <div class="form-group">
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs', 'display' => 'inline-block']) !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('id', $topic->id) !!}
                </div>
              {!! Form::close() !!}
            @endif
          </td>
        </tr>
      @endforeach
    </table>
  </div>
@endif
<table align="left" border="0" class="no-border-table">
  @if ($pageNo > 1 || count($topics) == 10)
    <tr>
        <td>
            @if($pageNo > 1)
                <p> <a href="/forum/{{$pageNo-1}}">Previous</a> </p>
            @endif
        </td>
        <td>
            @if(count($topics) == 10)
                <p> <a href="/forum/{{$pageNo+1}}">Next</a> </p>
            @endif
        </td>
    </tr>
  @endif
  <tr>
    <td>
      <caption align="bottom">
          {!! Form::open(['url' => 'forum/newTopic', 'method' => 'get']) !!}
          <div class="form-group">
            <a href="" data-toggle="modal" data-target="#createTopicModal" >
              {!! Form::submit('New Topic', ['class' => 'btn btn-primary form-control']) !!}
            </a>
          </div>
          {!! Form::close() !!}
      </caption>
    </td>
    <td></td>
  </tr>
</table>
@include('forum.topics.newTopic')
@stop
