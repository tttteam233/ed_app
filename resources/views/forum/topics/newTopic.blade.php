<div class="modal fade" id="createTopicModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <i class="fa fa-flash"></i>
          Add Topic
        </h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['url' => '/forum/newTopic']) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title: ') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
