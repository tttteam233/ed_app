@extends('layouts.application')
@section('styles')
  <link href="{{url('css/show_forum.css')}}" rel="stylesheet">
@stop
@section('content')
<div class="table-responsive">
  <table class="table">
    <tr>
      <th colspan="2">
        <h4>Comments</h4>
      </th>
    </tr>
    @if (count($comments) > 0)
      @foreach ($comments as $item)
        <tr>
          <td class="vertical-align-middle">{{$item->owner()->firstName}} {{$item->owner()->lastName}}</td>
          <td class="vertical-align-middle">
            <div class="comment-date">{{date("d M Y H:i",strtotime($item->created_at))}}</div>
            @if (Auth::user()->isAdmin() || Auth::user() == $item->owner())
              {!! Form::open(['url' => 'comments/delete', 'method' => 'post', 'align' => 'right']) !!}
                {!! Form::submit('Remove', ['class' => 'btn btn-danger btn-xs']) !!}
                {!! Form::hidden('id', $item->id) !!}
              {!! Form::close() !!}
            @endif
          </td>

        <tr>
          <td>
            <img src="{{$item->owner()->get_png_photo_path()}}" width="70px">
            <div class="{{$item->owner()->get_group_name()}}">{{$item->owner()->get_group_name()}}</div>
          </td>
          <td>{{ $item->comment }}</td>
        </tr>
      @endforeach
    @endif
    </table>
  </div>
<div class="post-reply" >
  <a href="" data-toggle="modal" data-target="#createCommentModal" class="btn btn-primary">
    Post Reply
  </a>
  <table align="left" border="0" class="no-border-table">
    <tr>
      <td>
        @if($pageNo > 1)
            <p> <a href="/forum/show/{{$topic->id}}/{{$pageNo-1}}">Previous</a> </p>
        @endif
      </td>
      <td>
        @if(count($comments) == 15)
            <p> <a href="/forum/show/{{$topic->id}}/{{$pageNo+1}}">Next</a> </p>
        @endif
      </td>
    </tr>
  </table>
</div>
@include("forum.add_comment_modal")
@stop
