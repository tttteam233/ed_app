<div class="modal fade" id="createCommentModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <i class="fa fa-flash"></i>
          Add Comment
        </h4>
      </div>
      <div class="modal-body">
        {!! Form::open() !!}
          <div class="form-group">
              {!! Form::label('comment', 'Comment: ') !!}
              {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
          </div>
          <div class="form-group">
              {!! Form::submit('Post', ['class' => 'btn btn-primary form-control']) !!}
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
