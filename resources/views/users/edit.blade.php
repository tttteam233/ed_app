@extends('layouts.application')
@section('styles')

@stop
@section('scripts')

@stop
@section('content')


{!! Form::model( $user, [ 'method'=> 'PUT', 'route' => ['user.update', $user->id]]) !!}
  <div class = "row" style="margin-top: 100px">
    <div class = "col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
    <div class = "col-lg-6 col-md-6 col-sm-8 col-xs-10 sign-up-content">
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-user-secret"> </i></span>
        <input value = "{{$user->username}}" placeholder="Registration Number" class = "form-control input-text" name="username">
      </div>
      <div class = "form-group input-group input-group-lg first-name">
        <span class="input-group-addon"><i class = "fa fa-user"> </i></span>
        <input value = "{{$user->firstName}}" placeholder="First Name" class = "form-control input-text" name="firstName">
      </div>
      <div class = "form-group input-group input-group-lg last-name">
        <span class="input-group-addon"><i class = "fa fa-user"> </i></span>
        <input value = "{{$user->lastName}}" placeholder="Last Name" class = "form-control input-text" name="lastName">
      </div>
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-envelope"> </i></span>
        <input value = "{{$user->email}}" placeholder="Email" class = "form-control input-text" name="email">
      </div>
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-lock"> </i></span>
        <input type="password" placeholder="Old Password" class = "form-control input-text" name="old_password" autocomplete="off">
      </div>
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-lock"> </i></span>
        <input type="password" placeholder="New Password" class = "form-control input-text" name="password" autocomplete="off">
      </div>
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-lock"> </i></span>
        <input type="password" placeholder="New Password Confirmation" class = "form-control input-text" name="password_confirmation" autocomplete="off">
      </div>

      {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
    </div>
    <div class = "col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
  </div>


{!! Form::close() !!}

@stop
