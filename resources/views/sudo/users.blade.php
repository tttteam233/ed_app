
@extends('layouts.application')
@section('styles')
  <link href="{{url('css/test.css')}}" rel="stylesheet">
@stop
@section('scripts')
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript">
    function deleteUser(userId){
        $.ajax({
            url: '/sudo/users/delete',
            type: 'POST',
            data: {id: userId, _token:'{{csrf_token()}}' },
            success: function(){
                location.reload();
            }
        })
    }
    function elevateUser(userId){
        $.ajax({
            url: '/sudo/users/elevate',
            type: 'POST',
            data: {id: userId, _token:'{{csrf_token()}}' },
            success: function(){
                location.reload();
            }
        })
    }
    function downgradeUser(userId){
        $.ajax({
            url: '/sudo/users/downgrade',
            type: 'POST',
            data: {id: userId, _token:'{{csrf_token()}}' },
            success: function(){
                location.reload();
            }
        })
    }
</script>
@stop
@section('content')
  <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 take_test">
      <div class="title">Users</div>
      <table>
        <tr style="border-top: 0px!important;">
          <th style="width: 30% !important; border-top: 0px!important;">Name</th>
          <th style="width: 30% !important; border-top: 0px!important;">Group</th>
          <th style="width: 40% !important; border-top: 0px!important;">Action</th>
        </tr>
        @foreach($users as $user)
          <tr>
            <td style="width: 30% !important; text-align: center">{{$user->firstName}} {{$user->lastName}}</td>
            <td style="width: 30% !important; text-align: center">{{$user->groupName}}</td>
            <td style="text-align: center">
              <button onclick="elevateUser({{$user->id}});" class="btn btn-info btn-sm">Elevate</button>
              <button onclick="downgradeUser({{$user->id}});" class="btn btn-primary btn-sm">Downgrade</button>
              <button onclick="deleteUser({{$user->id}});" class="btn btn-danger btn-sm">Delete</button>
            </td>
          </tr>
        @endforeach

      </table>
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
  </div>

@stop
