@extends('layouts.application')
@section('styles')
  <link href="{{url('css/test.css')}}" rel="stylesheet">
@stop
@section('scripts')
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript">
      function deleteCourse(topicId){
          $.ajax({
              url: "/sudo/topics/delete",
              type: "POST",
              data: {id: topicId, _token: '{{csrf_token()}}'},
              success: function(){
                  location.reload();
              }
          })
      }
  </script>
@stop
@section('content')
  <div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 take_test">
      <div class="title">Topics</div>
      <table>
        <tr style="border-top: 0px!important;">
          <th style="width: 60% !important; border-top: 0px!important;">Name</th>
          <th style="width: 20% !important; border-top: 0px!important;">Action</th>
        </tr>
        @foreach($topics as $topic)
          <tr>
            <td style="width: 60% !important; text-align: center">{{$topic->title}}</td>
            <td style="text-align: center"><button onclick="deleteCourse({{$topic->id}})" class="btn btn-danger btn-sm">Delete</button></td>
          </tr>
        @endforeach

      </table>
    </div>
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
  </div>

@stop
