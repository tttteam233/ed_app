@extends('layouts.application')
@section('styles')
  <link href="{{url('css/courses.css')}}" rel="stylesheet">
  <link href="{{url('css/dashboard.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
      <a href = "/sudo/users">
        <div class="boxCourse">
            <img src="images/user_admin_dashboard.png" alt="Add new Course"><br>
            <div class="down"> <a href="/sudo/users" class="btn btn-primary btn-xs" role="button" style="background-color: #2E99BD; border: 1px solid #2E99BD">User management</a></div>
        </div>
      </a>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
      <a href = "/sudo/courses">
        <div class="boxCourse">
            <img src="images/course_admin_dashboard.png" alt="Add new Course"><br>
            <div class="down"><a href="/sudo/courses" class="btn btn-primary btn-xs" role="button" style="background-color: #2E99BD; border: 1px solid #2E99BD">Course management</a></div>
        </div>
      </a>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
      <a href = "/sudo/topics">
        <div class="boxCourse">
            <img src="images/forum_admin_dashboard.png" alt="Add new Course"><br>
            <div class="down"><a href="/sudo/topics" class="btn btn-primary btn-xs" role="button" style="background-color: #2E99BD; border: 1px solid #2E99BD">Forum topics management</a></div>
        </div>
      </a>
    </div>
  <div>


@stop
