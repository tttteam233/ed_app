<!DOCTYPE html>
<html>
  <head>
    <title>TTTTeam</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{url('css/guest.css')}}" rel="stylesheet">
    @yield('styles')

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.js"></script>
  </head>
  <body>
    <div class="wrapper">
      <nav class="navbar navbar-default header">
        <div class="col-lg-2 col-md-2 col-sm-2"></div>
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-menu" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href = "/" class="logo">
              <img src="/images/logo_with_text.png">
            </a>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-1 move-right"></div>
          <div class="collapse navbar-collapse" id="collapse-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class = "menu-item menu-item-active"><a href="/">HOME</a></li>
                <li class = "menu-item"><a href="/about">ABOUT</a></li>
                  <li class = "menu-item"><a href="/auth/login">LOGIN</a></li>
                  <li class = "menu-item"><a href="/auth/register">SIGN UP</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="container">
          @yield('content')
      </div>
      <div class="footer">
        <img src="/images/logo.png" width="40px">
      </div>
    </div>
  </body>
</html>
