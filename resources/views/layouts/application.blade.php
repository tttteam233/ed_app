<!DOCTYPE html>
<html>
  <head>
    <title>TTTTeam</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{url('css/application.css')}}" rel="stylesheet">
    @yield('styles')

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.js"></script>
    @yield('scripts')

    <?php
      use App\Models\Course;
      use App\Models\Topic;
      use App\Models\User;
    ?>
  </head>
  <body>
    <div class="wrapper">
      <div class="left-navbar">
        <div class="user-and-avatar">
          <img src="{{Auth::user()->get_jpg_photo_path()}}" width="50px">
          <p class="name">{!! Auth::user()->firstName !!} {!! Auth::user()->lastName !!}</p>
        </div>

        <div id="main-menu">
          <div class="list-group panel">
            <div class="dashboard menu-item">
              @if (Auth::user()->isAdmin())
                <a href="/sudo" class="list-group-item">
                  <i class="fa fa-windows"></i>
                  Admin Dashboard
                </a>
              @else
                <a href="/dashboard" class="list-group-item">
                  <i class="fa fa-windows"></i>
                  Dashboard
                </a>
              @endif
            </div>
            <div class="menu-item">
              <a href="#list1" class="list-group-item" data-toggle="collapse" data-parent="#main-menu">
                <i class="fa fa-book"></i>
                Courses
              </a>
            </div>
            <div class="collapse" id="list1">
              <div class="subitem-title">
                @if (count(Course::all()) > 0)
                  <p>All Courses</p>
                @endif
              </div>
              @foreach (Course::all() as $course)
                <div class="subitem">
                  <a href="{{url('/courses',$course->id)}}" class="list-group-item">
                    <i class="fa fa-file-text-o"></i>
                    {!!$course->title!!}
                  </a>
                </div>
              @endforeach
              <div class="subitem-title">
                <p>Actions</p>
              </div>
              <div class="subitem">
                @if (Auth::user()->isStudent())
                  <a href="{{url('/dashboard')}}" class="list-group-item">
                    <i class="fa fa-book"></i>
                    View Courses
                  </a>
                @else
                  <a href="{{url('/courses/create')}}" class="list-group-item">
                    <i class="fa fa-plus"></i>
                    New Course
                  </a>
                @endif
              </div>
            </div>

            <div class="menu-item {{ Request::is('forum') ? 'active' : '' }}">
              <a href="/forum" class="list-group-item">
                <i class="fa fa-comments-o"></i>
                Forum
              </a>
            </div>

            <div class="menu-item {{ Request::is('quiz') ? 'active' : '' }}">
              @if (Auth::user()->isStudent())
                <a href="/studentGrades" class="list-group-item">
                  <i class="fa fa-flash"></i>
                  Quizzes
                </a>
              @else
                <a href="/teacherGrades" class="list-group-item">
                  <i class="fa fa-flash"></i>
                  Quizzes
                </a>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="header">
        <div class="header-options">
          <a href="/dashboard">
            <i class="fa fa-home fa-lg"></i>
          </a>
          <a href="{{route('user.edit', Auth::user()->id)}}">
            <i class="fa fa-user fa-lg"></i>
          </a>
          <a href="/auth/logout">
            <i class="fa fa-sign-out fa-lg"></i>
            Log out
          </a>
        </div>
      </div>
      <div class="content">
        @yield('content')
    </div>
      </div>
  </body>
</html>
