
@extends('layouts.application')
@section('styles')
  <link href="{{url('css/auth.css')}}" rel="stylesheet">
  <link href="{{url('css/courses.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">
@stop
@section('scripts')
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js"></script>
<script src="//cdn.ckeditor.com/4.5.6/full/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function () {
$(function () {
  $('#datetimepicker1').datetimepicker({
    format: 'YYYY/MM/DD HH:mm:ss'
    });
  $('#datetimepicker2').datetimepicker({
    format: 'YYYY/MM/DD HH:mm:ss'
    });
  });
});
</script>
@stop
@section('content')

  <div class = "row" style="margin-top: 70px; margin-bottom: 20px;">
    <div class = "col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
    <div class = "col-lg-8 col-md-8 col-sm-10 col-xs-10 sign-in-content", style="height: auto; padding-bottom: 50px">
      <h2>
        <i class="fa fa-book"></i>
        New Course
      </h2>
      <hr>
      @if(count($errors) > 0)
        <ul class="alert alert-danger">
          @foreach($errors->all() as $error)
            <li>
              {{$error}}
            </li>
          @endforeach
        </ul>
      @endif
      {!!Form::open(['url' => 'courses/{{$course->id}}'],['id'=>'form1'])!!}
        <div class="form-group">
            {!! Form::hidden('id',Auth::user()->id) !!}
        </div>
        <div class="form-group">
            {!! Form::hidden('idCourse',$course->id) !!}
        </div>
        <div class="form-group first no-top-margin">
          {!! Form::label('title','Title') !!}
          {!! Form::text('title',$course->title,['class' => 'form-control', 'autocomplete' => 'off']) !!}
        </div>
        <div class="form-group second">
          {!! Form::label('department','Department') !!}
          {!! Form::select('department',['Biology' => 'Biology', 'Chemistry' => 'Chemistry', 'Computer Science'=>'Computer Science', 'Geography' => 'Geography', 'Mathematics'=>'Mathematics'],$course->department,['class' => 'form-control', 'autocomplete' => 'off']) !!}
        </div>
        <div class="form-group first">
        {!! Form::label('start_date','Start-Date') !!}
        {!! Form::text('start_date',$course->start_date,['class' => 'form-control', 'id' => 'datetimepicker1', 'autocomplete' => 'off']) !!}
        </div>
        <div class="form-group second">
        {!! Form::label('end_date','End-Date') !!}
        {!! Form::text('end_date',$course->end_date,['class' => 'form-control', 'id' => 'datetimepicker2', 'autocomplete' => 'off']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('description','Description') !!}
          {!! Form::textarea('description',$course->description,['class' => 'form-control', 'autocomplete' => 'off']) !!}
        </div>
        <div class="form-group submit-button">
          {!! Form::submit('Update Course',['class' => 'btn btn-primary form-control']) !!}
        </div>
      {!! Form::close() !!}
    </div>
    <div class = "col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
  </div>
  <script>
    CKEDITOR.replace( 'description' );
    CKEDITOR.config.toolbar = 'Full';

    CKEDITOR.config.toolbar_Full =
    [
    	{ name: 'document', items : [ 'Source','-','Preview','Print','Templates' ] },
    	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
    	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
    	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
    	{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
            'HiddenField' ] },
    	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },

    	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
    	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
    	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
    	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
    	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
    	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] }
    ];
  </script>
@stop
