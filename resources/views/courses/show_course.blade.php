@extends('layouts.application')
@section('styles')
  <link href="{{url('css/show_course.css')}}" rel="stylesheet">
@stop
@section('content')
  <?php
    use App\Models\Course;
    use App\Models\Topic;
  ?>

  <div class="row padding-top" style="height: 550px">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="course-info">
          <div class="course-info-header">
            <div class="user-name">{{$teacher}}</div>
            <div class="courses-number">{{$course->title}}</div>
          </div>
          <div class="course-picture">
            <img src="{{$course->get_photo_path()}}" alt="Computer Graphics">
          </div>
          <div class="course-description">
            <div class="value"><?php echo $course->description;?></div>
          </div>
        </div>
      </div>

      <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
        <div class="course-fields">
          <div class="big-border"></div>
          <div class="field">Title</div>
          <i class="fa fa-puzzle-piece"></i>
          <div class="value">{{$course->title}}</div>
          <div class="small-border"></div>
          <div class="field">Department</div>
          <i class="fa fa-fort-awesome"></i>
          <div class="value">{{$course->department}}</div>
          <div class="small-border"></div>
          <div class="field">Start Date</div>
          <i class="fa fa-calendar"></i>
          <div class="value">{{date("d M Y H:i",strtotime($course->start_date))}}</div>
          <div class="small-border"></div>
          <div class="field">End Date</div>
          <i class="fa fa-calendar"></i>
          <div class="value">{{date("d M Y H:i",strtotime($course->end_date))}}</div>
        </div>
        <div class="course-options no-padding-top">
          @if (Auth::user()->isAdmin() || (Auth::user()->isTeacher() && Auth::user()->id == $course->user_id))
            <div class="hover-link">
              <a href="<?php echo '/courses/update/'.$course->id?>">
                <div class="field"></div>
                <div class="value big-padding">
                  <i class="fa fa-pencil-square-o"></i>
                  Edit course
                </div>
              </a>
            </div>
            <div class="hover-link">
              <a href="<?php echo '/courses/delete/'.$course->id?>">
                <div class="small-border"></div>
                <div class="field"></div>
                <div class="value big-padding">
                  <i class="fa fa-trash-o"></i>
                  Delete course
                </div>
              </a>
            </div>
            <div class="hover-link">
              <a href="" data-toggle="modal" data-target="#createLessonModal" >
                <div class="big-border"></div>
                <div class="field"></div>
                <div class="value big-padding">
                  <i class="fa fa-modx"></i>
                  Add new lesson
                </div>
              </a>
            </div>
            <div class="hover-link">
              <a href="" data-toggle="modal" data-target="#createQuizModal" >
                <div class="big-border"></div>
                <div class="field"></div>
                <div class="value big-padding">
                  <i class="fa fa-keyboard-o"></i>
                  Add new question
                </div>
              </a>
            </div>
          @endif
          @if (Auth::user()->isStudent() && !$isEnrolled)
            <div class="big-border"></div>
            {!!Form::open(['url' => 'courses/{id}/store-user'])!!}
                  {!! Form::hidden('id',Auth::user()->id) !!}
                  {!! Form::hidden('idCourse',$course->id) !!}
              <div class="form-group">
                  {!! Form::submit('Enroll me',['class' => 'btn btn-info form-control enrol-button']) !!}
              </div>
            {!! Form::close() !!}
          @endif
          @if (Auth::user()->isStudent() && $isEnrolled && !$hasGrade && $questions->count() > 0)
            <a href="/quiz/{{$course->id}}" class="btn btn-info enrol-button margin-left">Attend Quiz</a>
          @endif
        </div>
      </div>
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
    </div>
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
  </div>
  @if ($users)
    <div class="row">
      <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
        <div class="left-side line"></div>
        <div class="middle-side  participants-table">
          Participants
        </div>
        <div class="right-side line"></div>
      </div>
    </div>

    <table class="users-list">
      @foreach (array_chunk($users, 5) as $usersSection)
        <tr>
          @foreach($usersSection as $user)
            <td>
              <span>{{$user->firstName}} {{$user->lastName}}</span>
              @if (Auth::user()->isAdmin() || (Auth::user()->isTeacher() && Auth::user()->id == $course->user_id))
                <span class="matricol"> ({{$user->username}})</span>
              @endif
            </td>
          @endforeach
        </tr>
      @endforeach
    </table>
  @endif

  @if ($lessons->count() > 0)
    <div class="row">
      <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
        <div class="left-side line"></div>
        <div class="middle-side">
          Lessons
        </div>
        <div class="right-side line"></div>
      </div>
    </div>

    <table class="lessons-list">
      @foreach($lessons as $lesson)
        <tr>
          @if (Auth::user()->isAdmin() || (Auth::user()->isTeacher() && Auth::user()->id == $course->user_id))
            <td>
              <span>{!!str_limit($lesson->description,100)!!}</span>
              <span><a href="/courses/{{$course->id}}/{{$lesson->id}}">More</a></span>
            </td>
          @else
            <td class="full-width">
              <span>{!!str_limit($lesson->description,100)!!}</span>
              <span><a href="/courses/{{$course->id}}/{{$lesson->id}}">More</a></span>
            </td>
          @endif
          <td>
            @if (Auth::user()->isAdmin() || (Auth::user()->isTeacher() && Auth::user()->id == $course->user_id))
              <a href="/courses/{{$course->id}}/update/{{$lesson->id}}" class="btn btn-info btn-xs">Edit</a>
              <a href="/courses/{{$course->id}}/delete/{{$lesson->id}}" class="btn btn-danger btn-xs">Delete</a>
            @endif
          </td>
        </tr>
      @endforeach
    </table>
  @endif

  @if ($questions->count() > 0 && Auth::user()->isAdmin() || (Auth::user()->isTeacher() && Auth::user()->id == $course->user_id))
    <div class="row">
      <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
        <div class="left-side line"></div>
        <div class="middle-side">
          Questions
        </div>
        <div class="right-side line"></div>
      </div>
    </div>

    <table class="questions-list">
      @foreach($questions as $question)
        <tr>

          <td>
            <b>{!!$question->name!!}</b>
          </td>
          @if($question->answer == $question->answer1)
            <td class="right-answer">
              <b>{!!$question->answer1!!}</b>
            </td>
          @else
            <td>
              {!!$question->answer1!!}
            </td>
          @endif
          @if($question->answer == $question->answer2)
            <td class="right-answer">
              <b>{!!$question->answer2!!}</b>
            </td>
          @else
            <td>
              {!!$question->answer2!!}
            </td>
          @endif
          @if($question->answer == $question->answer3)
            <td class="right-answer">
              <b>{!!$question->answer3!!}</b>
            </td>
          @else
            <td>
              {!!$question->answer3!!}
            </td>
          @endif
          @if($question->answer == $question->answer4)
            <td class="right-answer">
              <b>{!!$question->answer4!!}</b>
            </td>
          @else
            <td>
              {!!$question->answer4!!}
            </td>
          @endif
        </tr>
      @endforeach
    </table>
  @endif

  @include('quiz.create')
  @include('lessons.create')
@stop
