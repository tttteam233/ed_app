@extends('layouts.application')
@section('styles')
  <link href="{{url('css/courses.css')}}" rel="stylesheet">
@stop
@section('content')
    <h1>Courses</h1>
    <hr/>
    <a href="<?php echo 'courses/create'?>" >Create new course</a>
    @foreach($courses as $course)
        <article>
            <h2>
                <a href="{{url('/courses',$course->id)}}">{{$course->title}}</a>
            </h2>
            <div class="description">
                <?php echo $course->description;?>
            </div>
            <div class="department">
                {{$course->department}}
            </div>
            <div class="start_date">
                {{$course->start_date}}
            </div>
            <div class="end_date">
                {{$course->end_date}}
            </div>
            <a href="<?php echo 'courses/delete/'.$course->id?>" >Delete course</a>
        </article>

    @endforeach

@stop
