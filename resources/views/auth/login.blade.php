@extends('layouts.guest')
@section('styles')
  <link href="{{url('css/auth.css')}}" rel="stylesheet">
@stop
@section('content')
<div class="higher-logo">
  <a href = "/" >
    <img src="/images/logo_black_text.png">
  </a>
</div>
@if(count($errors) > 0)
    <ul class="alert alert-danger">
        @foreach($errors->all() as $error)
            <li>
                {{$error}}
            </li>
        @endforeach
    </ul>
@endif
{!! Form::open(['id' => 'loginForm', 'method' => 'POST']) !!}
<div class = "row">
  <div class = "col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
  <div class = "col-lg-6 col-md-6 col-sm-8 col-xs-10 sign-in-content">
    <div class = "form-group input-group input-group-lg sign-in-input">
      <span class="input-group-addon"><i class = "fa fa-envelope"> </i></span>
      <input placeholder="Email" class = "form-control input-text" name="email">
    </div>
    <div class = "form-group input-group input-group-lg">
      <span class="input-group-addon"><i class = "fa fa-lock"> </i></span>
      <input type="password" placeholder="Password" class = "form-control input-text" name="password">
    </div>
    <div class="button">
      <button type="button" class="btn btn-default btn-lg" onclick="document.getElementById('loginForm').submit();">
        SIGN IN
      </button>
    </div>
  </div>
  <div class = "col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
</div>
    {!! Form::close() !!}
@stop
