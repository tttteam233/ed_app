@extends('layouts.guest')
@section('styles')
  <link href="{{url('css/auth.css')}}" rel="stylesheet">
@stop
@section('content')

  <div class="higher-logo">
    <a href = "/" >
      <img src="/images/logo_black_text.png">
    </a>
  </div>
  {!! Form::open(['id' => 'registerForm', 'method' => 'POST']) !!}
  <div class = "row">
    <div class = "col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
    <div class = "col-lg-6 col-md-6 col-sm-8 col-xs-10 sign-up-content">
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-user-secret"> </i></span>
        <input placeholder="Registration Number" class = "form-control input-text" name="username">
      </div>
      <div class = "form-group input-group input-group-lg first-name">
        <span class="input-group-addon"><i class = "fa fa-user"> </i></span>
        <input placeholder="First Name" class = "form-control input-text" name="firstName">
      </div>
      <div class = "form-group input-group input-group-lg last-name">
        <span class="input-group-addon"><i class = "fa fa-user"> </i></span>
        <input placeholder="Last Name" class = "form-control input-text" name="lastName">
      </div>
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-envelope"> </i></span>
        <input placeholder="Email" class = "form-control input-text" name="email">
      </div>
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-lock"> </i></span>
        <input type="password" placeholder="Password" class = "form-control input-text" name="password">
      </div>
      <div class = "form-group input-group input-group-lg">
        <span class="input-group-addon"><i class = "fa fa-lock"> </i></span>
        <input type="password" placeholder="Password Confirmation" class = "form-control input-text" name="password_confirmation">
      </div>
      <div class="terms">
        <!-- <input type="checkbox" name="terms_and_conditions" required><span>I have read and agree to the </span><a data-toggle="modal" data-target="#termsModal">Terms & Conditions</a> -->
      </div>
      <div class="button">
        <button type="button" class="btn btn-default btn-lg" onclick="document.getElementById('registerForm').submit();">
          SIGN UP
        </button>
      </div>
    </div>
    <div class = "col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
  </div>
  {!! Form::close() !!}
  @include('auth.terms_modal')
@stop
