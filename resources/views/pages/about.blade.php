@extends('layouts.guest')
@section('styles')
  <link href="{{url('css/auth.css')}}" rel="stylesheet">
  <link href="{{url('css/about.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class="higher-logo" style="height: 70px">
  </div>
  <div class = "row">
    <div class = "col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
    <div class = "col-lg-8 col-md-8 col-sm-10 col-xs-10 sign-in-content">
      <div class="header-about">
        The Benefits of eLearning
      </div>
      <div class="description-about">
        There are four key benefits in which eLearning has transformed the landscape of learning and development. When compared to the traditional mode of classroom learning, there is clear evidence that eLearning brings:
      </div>
      <div class="list-of-item-about">
        <div class="item-about">1. Faster delivery</div>
        <div class="item-about">2. Lower costs</div>
        <div class="item-about">3. More effective learning</div>
        <div class="item-about">4. Lower environmental impact</div>
      </div>
      <div class="citat-about">
        “Never stop learning, because life never stop teaching.”
      </div>
    </div>
    <div class = "col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
  </div>
@stop
