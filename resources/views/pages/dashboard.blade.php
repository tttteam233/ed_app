@extends('layouts.application')
@section('styles')
  <link href="{{url('css/dashboard.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      @if (Auth::user()->isTeacher() || Auth::user()->isAdmin())
        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <a href = "/courses/create">
            <div class="boxCourse">
                <img src="images/add.png" alt="Add new Course"><br>
                <div class="down"><a href="/courses/create" class="btn btn-primary btn-xs" role="button">Add new course</a></div>
            </div>
          </a>
        </div>
      @endif
      @foreach ($all_courses as $course)
        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
            <div class="boxCourse">
                <a href="{{url('/courses',$course->id)}}">
                  <img src="{{$course->get_photo_path()}}" alt="Computer Graphics"><br>
                </a>
                <b style="font-size:17px;">{!! $course->title !!}</b><br>
                <b>End Date:</b>
                {!!date("d M Y H:i",strtotime($course->end_date))!!}<br><br>
                @if (Auth::user()->isStudent() && !Auth::user()->is_enrolled($course->id))
                  {!!Form::open(['url' => 'courses/{$course->id}/store-user'])!!}
                    {!! Form::hidden('id',Auth::user()->id) !!}
                    {!! Form::hidden('idCourse',$course->id) !!}
                    <div class="form-group">
                        {!! Form::submit('Enroll me',["class" => "btn btn-info btn-xs enroll-btn"]) !!}
                    </div>
                  {!! Form::close() !!}
                @endif
            </div>
        </div>
      @endforeach
    </div>
@stop
