@stop
@extends('layouts.application')
@section('styles')
  <link href="{{url('css/test.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class="row">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 take_test">
      <div class="title">{{$course->title}}</div>
      <table>
        <tr>
          <th>Question</th>
          <th>Answer 1</th>
          <th>Answer 2</th>
          <th>Answer 3</th>
          <th>Answer 4</th>
        </tr>
        {!!Form::open()!!}
          <?php $questionNo = 1;?>
          @foreach($questions as $question)
            <tr>
              <td><label name="q{{$questionNo}}">{{$question['name']}}</label></td>
              <?php $answerNumber = 1 ?>
              @for($i = 0 ; $i < 4; $i++)
                <td><input type="radio" name="q{{$questionNo}}" value="{{$question['answer'.$answerNumber] == $question['answer'] ? 1 : 0}}">{{$question['answer'.$answerNumber]}}</td>
                <?php $answerNumber++; ?>
              @endfor
              <?php $questionNo++; ?>
            </tr>
          @endforeach
          {!!Form::Submit('Submit Answers', ['class' => 'btn btn-info submit-btn form-control'])!!}
        {!!Form::close()!!}
      </table>
    </div>
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
  </div>

@stop
