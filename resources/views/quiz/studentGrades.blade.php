@stop
@extends('layouts.application')
@section('styles')
  <link href="{{url('css/student_grades.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">
  <script src="//cdn.ckeditor.com/4.5.6/full/ckeditor.js"></script>
@stop
@section('content')
  <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 student_grades">
      <div class="title">Catalogue</div>
      @if ($grades)
        <table>
          <tr>
            <th>Course</th>
            <th>Grade</th>
            <th>Passed</th>
          </tr>
          @foreach($grades as $grade)
            <tr>
              <td>{{$grade->title}}</td>
              <td>{{$grade->grade}}</td>
              @if ($grade->grade < 5)
                <td class="red"><b>FALSE</b></td>
              @else
                <td class="green"><b>TRUE</b></td>
              @endif
            </tr>
          @endforeach
        </table>
      @endif
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
  </div>
@stop
