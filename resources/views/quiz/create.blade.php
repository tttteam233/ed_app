<head>
  <link href="{{url('css/quizzes.css')}}" rel="stylesheet">
</head>

<div class="modal fade" id="createQuizModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <i class="fa fa-flash"></i>
          Add Question
        </h4>
      </div>
      <div class="modal-body">
        {!!Form::open(['url' => 'courses/'.$course->id.'/quizzes/create'])!!}
        <div class="row">
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div class="form-group">
               {!! Form::label('name','Question') !!}
               {!! Form::text('name',null,['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="form-group">
               {!! Form::label('answer1','Answer 1') !!}
               {!! Form::text('answer1',null,['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="form-group">
               {!! Form::label('answer2','Answer 2') !!}
               {!! Form::text('answer2',null,['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="form-group">
               {!! Form::label('answer3','Answer 3') !!}
               {!! Form::text('answer3',null,['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="form-group">
               {!! Form::label('answer4','Answer 4') !!}
               {!! Form::text('answer4',null,['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="form-group">
               {!! Form::label('answer','Correct Answer') !!}
               {!! Form::text('answer',null,['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
              <div class="form-group">
                  {!! Form::hidden('course_id',$course->id) !!}
              </div>
              <div class="form-group submit-button">
                  {!! Form::submit('Add Question',['class' => 'btn btn-primary form-control']) !!}
              </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
          </div>
          {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
