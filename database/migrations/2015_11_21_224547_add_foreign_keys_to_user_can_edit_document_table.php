<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserCanEditDocumentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_can_edit_document', function(Blueprint $table)
		{
			$table->foreign('Document_id', 'fk_User_has_Document_Document1')->references('id')->on('document')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('User_id', 'fk_User_has_Document_User1')->references('id')->on('user')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_can_edit_document', function(Blueprint $table)
		{
			$table->dropForeign('fk_User_has_Document_Document1');
			$table->dropForeign('fk_User_has_Document_User1');
		});
	}

}
