<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentCanBeViewedByUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_can_be_viewed_by_user', function(Blueprint $table)
		{
			$table->integer('Document_id')->index('fk_Document_has_User_Document1_idx');
			$table->integer('User_id')->index('fk_Document_has_User_User1_idx');
			$table->dateTime('start_date')->nullable();
			$table->dateTime('end_date')->nullable();
			$table->string('can_be_viewed', 45)->nullable();
			$table->primary(['Document_id','User_id']);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_can_be_viewed_by_user');
	}

}
