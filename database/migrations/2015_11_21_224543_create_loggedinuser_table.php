<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoggedinuserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('loggedinuser', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('User_id')->nullable()->index('fk_LoggedInUser_User1_idx');
			$table->integer('Room_id')->nullable()->index('fk_LoggedInUser_Room1_idx');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('loggedinuser');
	}

}
