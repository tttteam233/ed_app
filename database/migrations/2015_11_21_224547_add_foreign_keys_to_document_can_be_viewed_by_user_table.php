<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentCanBeViewedByUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_can_be_viewed_by_user', function(Blueprint $table)
		{
			$table->foreign('Document_id', 'fk_Document_has_User_Document1')->references('id')->on('document')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('User_id', 'fk_Document_has_User_User1')->references('id')->on('user')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_can_be_viewed_by_user', function(Blueprint $table)
		{
			$table->dropForeign('fk_Document_has_User_Document1');
			$table->dropForeign('fk_Document_has_User_User1');
		});
	}

}
