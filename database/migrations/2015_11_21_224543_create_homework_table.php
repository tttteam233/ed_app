<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHomeworkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('homework', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 45)->nullable();
			$table->string('description', 45)->nullable();
			$table->dateTime('deadline')->nullable();
			$table->string('points', 45)->nullable();
			$table->string('group_type', 45)->nullable();
			$table->string('type', 45)->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('homework');
	}

}
