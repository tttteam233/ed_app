<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserHasTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_has_test', function(Blueprint $table)
		{
			$table->integer('User_id')->index('fk_User_has_Test_User1_idx');
			$table->integer('Test_id')->index('fk_User_has_Test_Test1_idx');
			$table->primary(['User_id','Test_id']);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_has_test');
	}

}
