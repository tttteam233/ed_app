<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('quiz', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('answer1');
          $table->string('answer2');
          $table->string('answer3');
          $table->string('answer4');
          $table->string('answer');
          $table->integer('course_id')->references('id')->on('course')->onDelete('cascade');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('quiz');
    }
}
