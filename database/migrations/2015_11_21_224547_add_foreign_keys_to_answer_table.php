<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAnswerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('answer', function(Blueprint $table)
		{
			$table->foreign('Homework_id', 'fk_Answer_Homework1')->references('id')->on('homework')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('answer', function(Blueprint $table)
		{
			$table->dropForeign('fk_Answer_Homework1');
		});
	}

}
