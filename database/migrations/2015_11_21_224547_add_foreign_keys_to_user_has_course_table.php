<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserHasCourseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_has_course', function(Blueprint $table)
		{
			$table->foreign('Course_id', 'fk_User_has_Course_Course1')->references('id')->on('course')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('User_id', 'fk_User_has_Course_User1')->references('id')->on('user')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_has_course', function(Blueprint $table)
		{
			$table->dropForeign('fk_User_has_Course_Course1');
			$table->dropForeign('fk_User_has_Course_User1');
		});
	}

}
