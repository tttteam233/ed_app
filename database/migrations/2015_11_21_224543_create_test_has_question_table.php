<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTestHasQuestionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('test_has_question', function(Blueprint $table)
		{
			$table->integer('Test_id')->index('fk_Test_has_Question_Test1_idx');
			$table->integer('Question_id')->index('fk_Test_has_Question_Question1_idx');
			$table->primary(['Test_id','Question_id']);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('test_has_question');
	}

}
