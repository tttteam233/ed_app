<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserHasTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_has_test', function(Blueprint $table)
		{
			$table->foreign('Test_id', 'fk_User_has_Test_Test1')->references('id')->on('test')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('User_id', 'fk_User_has_Test_User1')->references('id')->on('user')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_has_test', function(Blueprint $table)
		{
			$table->dropForeign('fk_User_has_Test_Test1');
			$table->dropForeign('fk_User_has_Test_User1');
		});
	}

}
