<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserCanEditCourseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_can_edit_course', function(Blueprint $table)
		{
			$table->integer('User_id')->index('fk_User_has_Course1_User1_idx');
			$table->integer('Course_id')->index('fk_User_has_Course1_Course1_idx');
			$table->primary(['User_id','Course_id']);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_can_edit_course');
	}

}
