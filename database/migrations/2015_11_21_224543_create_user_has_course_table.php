<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserHasCourseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_has_course', function(Blueprint $table)
		{
			$table->integer('User_id')->index('fk_User_has_Course_User1_idx');
			$table->integer('Course_id')->index('fk_User_has_Course_Course1_idx');
			$table->primary(['User_id','Course_id']);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_has_course');
	}

}
