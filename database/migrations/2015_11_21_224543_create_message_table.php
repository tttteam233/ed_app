<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('text', 45);
			$table->integer('user_id')->index('fk_Message_User1_idx');
			$table->integer('to_user_id')->index('fk_Message_User2_idx');
			$table->integer('Room_id')->nullable()->index('fk_Message_Room1_idx');
			$table->primary(['id','user_id','to_user_id']);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message');
	}

}
