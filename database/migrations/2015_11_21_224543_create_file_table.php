<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('file', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('path', 200);
			$table->string('type', 45);
			$table->dateTime('added_at');
			$table->integer('User_id')->nullable()->index('fk_File_User1_idx');
			$table->integer('Document_id')->nullable()->index('fk_File_Document1_idx');
			$table->integer('Course_id')->nullable()->index('fk_File_Course1_idx');
			$table->integer('Comment_id')->nullable()->index('fk_File_Comment1_idx');
			$table->integer('Question_id')->nullable()->index('fk_File_Question1_idx');
			$table->dateTime('start_date')->nullable();
			$table->dateTime('end_date')->nullable();
			$table->integer('Homework_id')->nullable()->index('fk_File_Homework1_idx');
			$table->integer('Answer_id')->nullable()->index('fk_File_Answer1_idx');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('file');
	}

}
