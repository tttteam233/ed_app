<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTestHasQuestionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('test_has_question', function(Blueprint $table)
		{
			$table->foreign('Question_id', 'fk_Test_has_Question_Question1')->references('id')->on('question')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Test_id', 'fk_Test_has_Question_Test1')->references('id')->on('test')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('test_has_question', function(Blueprint $table)
		{
			$table->dropForeign('fk_Test_has_Question_Question1');
			$table->dropForeign('fk_Test_has_Question_Test1');
		});
	}

}
