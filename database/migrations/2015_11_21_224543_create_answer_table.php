<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnswerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('answer', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 45)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('type', 45)->nullable();
			$table->integer('Homework_id')->nullable()->index('fk_Answer_Homework1_idx');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('answer');
	}

}
