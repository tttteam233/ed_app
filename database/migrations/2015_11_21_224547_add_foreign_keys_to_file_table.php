<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('file', function(Blueprint $table)
		{
			$table->foreign('Answer_id', 'fk_File_Answer1')->references('id')->on('answer')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Course_id', 'fk_File_Course1')->references('id')->on('course')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Document_id', 'fk_File_Document1')->references('id')->on('document')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Homework_id', 'fk_File_Homework1')->references('id')->on('homework')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('Question_id', 'fk_File_Question1')->references('id')->on('question')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('User_id', 'fk_File_User1')->references('id')->on('user')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('file', function(Blueprint $table)
		{
			$table->dropForeign('fk_File_Answer1');
			$table->dropForeign('fk_File_Course1');
			$table->dropForeign('fk_File_Document1');
			$table->dropForeign('fk_File_Homework1');
			$table->dropForeign('fk_File_Question1');
			$table->dropForeign('fk_File_User1');
		});
	}

}
