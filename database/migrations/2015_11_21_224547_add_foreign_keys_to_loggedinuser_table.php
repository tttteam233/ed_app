<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLoggedinuserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('loggedinuser', function(Blueprint $table)
		{
			$table->foreign('Room_id', 'fk_LoggedInUser_Room1')->references('id')->on('room')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('User_id', 'fk_LoggedInUser_User1')->references('id')->on('user')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('loggedinuser', function(Blueprint $table)
		{
			$table->dropForeign('fk_LoggedInUser_Room1');
			$table->dropForeign('fk_LoggedInUser_User1');
		});
	}

}
