<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('test', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 45);
			$table->text('description', 65535);
			$table->dateTime('deadline');
			$table->dateTime('start_date');
			$table->dateTime('end_date');
			$table->string('password', 45)->nullable();
			$table->string('type', 45)->nullable();
			$table->string('group_type', 45)->nullable();
			$table->string('last_note', 45)->nullable();
			$table->string('max_note', 45)->nullable();
			$table->string('access_links', 45)->nullable();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('test');
	}

}
