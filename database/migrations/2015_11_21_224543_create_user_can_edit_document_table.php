<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserCanEditDocumentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_can_edit_document', function(Blueprint $table)
		{
			$table->integer('User_id')->index('fk_User_has_Document_User1_idx');
			$table->integer('Document_id')->index('fk_User_has_Document_Document1_idx');
			$table->primary(['User_id','Document_id']);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_can_edit_document');
	}

}
