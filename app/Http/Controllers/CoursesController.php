<?php
/**
 * Created by PhpStorm.
 * User: Bianca Ianc
 * Date: 11/23/2015
 * Time: 10:48 PM
 */

namespace App\Http\Controllers;
use App\Models\Course;
use App\Models\Grade;
use App\Models\Lesson;
use App\Models\Quiz;
use App\Models\User;
use App\Models\UserHasCourse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class CoursesController extends Controller
{

    public function index()
    {
        $all_courses = Course::all();
        return view('pages.dashboard', compact('all_courses'));
    }

    public function show_course($id)
    {
        $course = Course::find($id);
        $users_have_course = UserHasCourse::where('course_id', $id)->get();
        $users = array();
        $lessons = Lesson::where('course_id',$id)->get();
        $questions = Quiz::where('course_id',$id)->get();
        $isEnrolled = UserHasCourse::where('course_id', $id)->where('user_id','=',Auth::user()['id'])->count() == 1;
        $hasGrade = Grade::where('user_id','=',Auth::user()['id'])->where('course_id','=',$id)->count() == 1;
        $teacher = User::find($course->user_id)->firstName.' '.User::find($course->user_id)->lastName;
        foreach ($users_have_course as $user_has_course) {
            $user = User::find($user_has_course->user_id);
            array_push($users, $user);
        }
        if (is_null($course)) {
            abort(404);
        } else {
            return view('courses.show_course', compact('course', 'users','lessons','questions','isEnrolled','hasGrade','teacher'));
        }
    }

    public function create()
    {
        return view('courses.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required|min:3', 'description' => 'required', 'start_date' => 'required|date', 'end_date' => 'required|date', 'department' => 'required']);
        $request['created_at'] = Carbon::now();
        $request['updated_at'] = Carbon::now();
        $request['user_id'] = Auth::user()['id'];
        Course::create($request->all());
        return redirect('dashboard');
    }

    public function delete($id)
    {
        $course = Course::find($id);
        if (is_null($course)) {
            abort(404);
        } else {
            $course->delete();
            return redirect('dashboard');
        }
    }
    public function update_course($id){
        $course = Course::find($id);
        $users_have_course = UserHasCourse::where('course_id', $id)->get();
        $users = array();
        foreach ($users_have_course as $user_has_course) {
            $user = User::where('id', $user_has_course->user_id)->get();
            array_push($users, $user);
        }
        $lessons = Lesson::where('course_id',$id)->get();
        if (is_null($course)) {
            abort(404);
        } else {
            return view('courses.update', compact('course', 'users','lessons'));
        }


    }
    public function save(Request $request){
        $course = Course::find($request['idCourse']);
        $course->title = $request['title'];
        $course->department = $request['department'];
        $course->description = $request['description'];
        $course->save();
        return redirect('/courses/'.$request['idCourse']);
    }
    public function store_user(Request $request)
    {
        $id = $request['id'];
        $users = User::where('id', $id)->get();
        $idCourse = $request['idCourse'];
        $user_is_enroled = 0;
        foreach ($users as $u) {
            $idUser = $u->id;
            $add['user_id'] = $idUser;
            $add['course_id'] = $idCourse;
            $user_exists = UserHasCourse::where('user_id', $idUser)->get();
            foreach ($user_exists as $u_e) {
                if ($u_e->course_id == $idCourse) {
                    $user_is_enroled = 1;
                }
            }
                if ($user_is_enroled == 0) {
                    UserHasCourse::create($add);
                    return redirect('courses/' . $idCourse);
                }
            else return 'You are already a participant of this course';
            }
    }
}
