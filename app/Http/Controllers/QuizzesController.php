<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Grade;
use App\Models\Quiz;
use App\Models\Course;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class QuizzesController extends Controller
{
    public function index()
    {
        $all_quizzes = Quiz::all();
        return view('quiz.index', compact('all_quizzes'));
    }


    public function store(Request $request)
    {
        // $this->validate($request, ['name' => 'required|min:3', 'answer1' => 'required|min:1', 'answer2' => 'required|min:1', 'answer3' => 'required|min:1', 'answer4' => 'required|min:1'], 'answer' => 'required|min:1']);
        $request['created_at'] = Carbon::now();
        $request['updated_at'] = Carbon::now();
        Quiz::create($request->all());
        $redirectPath = "/courses/".$request['course_id'];
        return redirect($redirectPath);
    }

    public function takeTest($courseId){
        $questions = Quiz::where("course_id", "=", $courseId)->orderByRaw("RAND()")->take(10)->get();
        $course = Course::find($courseId);
        return view('quiz.test',compact('questions','course'));
    }

    public function grade($courseId, Request $request){
        $answers = $request->only(['q1','q2','q3','q4','q5','q6','q7','q8','q9','q10']);
        $points = 0;
        $questionNo = 0;
        foreach($answers as $answer){
            $points += $answer;
            if(isset($answer)) {
                $questionNo++;
            }
        }
        $grade = (floatval($points)/floatval($questionNo))*10;
        Grade::create(['grade'=>floatval(number_format($grade,2)),'user_id'=>Auth::user()['id'],'course_id'=>$courseId]);
        return redirect("/studentGrades");
    }

    public function getStudentGrades(){
        $grades = DB::table('grades')->
            join('course','course.id','=','grades.course_id')->
            select('course.title','grades.grade')->
            where('grades.user_id','=',Auth::user()['id'])->get();
        return view('quiz.studentGrades',compact('grades'));
    }

    public function getTeacherGrades(){
        $grades = DB::table('grades')->
            join('user','user.id','=','grades.user_id')->
            join('course','course.id','=','grades.course_id')->
            where('course.user_id','=',Auth::user()['id'])->
            select('user.firstName','user.lastName','course.title as courseTitle','grades.grade')->get();
        return view('quiz.teacherGrades',compact('grades'));
    }
}
