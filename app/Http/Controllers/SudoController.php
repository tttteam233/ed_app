<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Course;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SudoController extends Controller
{
    public function __construct(){
        $this->middleware('sudo');
    }

    public function index(){
        return view('sudo.index');
    }

    public function showUsers(){

        $users = DB::table('user')
            ->join('user_groups', 'usergroup_id','=','user_groups.id')
            ->select('user.id','user.firstName','user.lastName','user_groups.name as groupName')->get();

        return view("sudo.users", ['users' => $users]);
    }

    public function showCourses(){
        $courses = Course::all();
        return view('sudo.courses', ['courses' => $courses]);
    }

    public function showTopics(){
        $topics = Topic::all();
        return view('sudo.topics', ['topics' => $topics]);
    }

    public function deleteUser(Request $request){
        $id = $request['id'];
        $user = User::find($id);
        $user->delete();
    }

    public function elevateUser(Request $request){
        $id = $request['id'];
        $user = User::find($id);
        if($user->usergroup_id < 3){
            $user->usergroup_id += 1;
        }
        $user->save();
    }
    public function downgradeUser(Request $request){
        $id = $request['id'];
        $user = User::find($id);
        if($user->usergroup_id > 1){
            $user->usergroup_id -= 1;
        }
        $user->save();
    }

    public function deleteCourse(Request $request){
        $id = $request['id'];
        $course = Course::find($id);
        $course->delete();
    }

    public function deleteTopic(Request $request){
        $id = $request['id'];
        $topic = Topic::find($id);
        $comments = $topic->comments();
        foreach($comments as $comment){
            $comments->delete();
        }
        $topic->delete();
    }
}
