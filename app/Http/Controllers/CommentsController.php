<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentRequest;
use App\Models\Comment;
use App\Models\Topic;
//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;

class CommentsController extends Controller
{
    public function showComments($id)
    {
        return $this->pages($id,null);
    }

    public function pages($id, $pageNo)
    {
        if (!isset($pageNo)) {
            $pageNo = 1;
        }
        $topic = Topic::find($id);
        $comments = $topic->comments()->take(15)->skip(($pageNo - 1) * 15)->get();
//        return $comments[0]['comment'];
        return view('forum.show2', ['comments' => $comments, 'topic' => $topic, 'pageNo' => $pageNo ] );
    }

    public function addComment($id, CreateCommentRequest $request)
    {

        $redirectPath = "/forum/show/{$id}";
        $input        = Request::all();
        Comment::create(['topic_id' => $id, 'owner' => \Auth::user()['id'], 'comment' => $input['comment']]);
        return redirect($redirectPath);

    }

    public function delete()
    {
        $input        = Request::all();
        $comment      = Comment::find($input['id']);
        $redirectPath = '/forum/show/'.$comment['topic_id'];
        $comment->delete();
        return redirect($redirectPath);
    }

}
