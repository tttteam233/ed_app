<?php

namespace App\Http\Controllers;

use App\Models\Topic;
//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Request;

class TopicController extends Controller
{
    public function index(){
        return $this->page(null);
    }

    public function page($pageNo){
        if(!isset($pageNo))
        {
            $pageNo = 1;
        }

        $topics = Topic::orderBy('created_at', 'DESC')->take(10)->skip(($pageNo-1)*10)->get();
        return view('forum.index2', ['topics' => $topics, 'pageNo' => $pageNo ]);
    }

    public function newTopic()
    {
        return view('forum/topics.newTopic', ['user' => \Auth::user()]);
    }

    public function createTopic(Requests\CreateTopicRequest $request)
    {

        $input = Request::all();
        $owner = Auth::user();
        $input['owner'] = $owner['id'];
        Topic::create(['title' => $input['title'], 'owner' => $input['owner']]);
        return redirect('/forum');
    }

    public function delete()
    {
        $input = Request::all();
        $topic = Topic::find($input['id']);
        $topic->delete();
        return redirect('/forum');

    }
}
