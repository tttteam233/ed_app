<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Course;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Input;
use Hash;
class UsersController extends Controller
{

    public function edit($id)
    {
      if (Auth::user()->id == $id){
        $user = User::find($id);
        return view('users.edit', compact("user"));
      }
      else {
        if (Auth::user()->isAdmin()){
          return view('sudo/index', compact('all_courses'));
        }
        else{
          return view('pages/dashboard', compact('all_courses'));
        }
      }
    }

    public function update()
    {
      $user = Auth::user();
      $input = array_except(Input::all(), ['_method', '_token', 'old_password', 'password', 'password_confirmation']);
      $user->update($input);
      if (Hash::check(Input::get("old_password"), $user->password) && Input::get("password") == Input::get("password_confirmation")){
        $user->password = Hash::make(Input::get("password"));
        $user->save();
      }
      return redirect('/dashboard');
    }


}
