<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Course;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
  public function dashboard()
  {
    if (Auth::check()){
      $all_courses = Course::all();
      if (Auth::user()->isAdmin()){
        return view('sudo/index', compact('all_courses'));
      }
      else{
        return view('pages/dashboard', compact('all_courses'));
      }
    }
    else {
      return redirect('/auth/login');
    }
  }

  public function about()
  {
    return view('pages/about');
  }
}
