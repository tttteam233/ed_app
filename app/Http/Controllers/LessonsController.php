<?php
/**
 * Created by PhpStorm.
 * User: Bianca Ianc
 * Date: 1/10/2016
 * Time: 12:22 AM
 */

namespace App\Http\Controllers;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\User;
use App\Models\UserHasCourse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class LessonsController extends Controller{
    public function create(Request $request)
    {
        $url = $request->url();
        $parts = Explode('/', $url);
        $course_id = $parts[4];
        return view('lessons.create',compact('course_id'));
    }
    public function store_lesson(Request $request){
        $request['created_at'] = Carbon::now();
        $request['updated_at'] = Carbon::now();
        Lesson::create($request->all());
        return redirect('courses/'.$request['course_id']);
    }
    public function show_lesson(Request $request){
        $url = $request->url();
        $parts = Explode('/', $url);
        $lesson_id = $parts[5];
        $course_id = $parts[4];
        $lesson = Lesson::find($lesson_id);
        return view('lessons.show_lesson',compact('lesson','course_id'));
    }
    public function delete(Request $request){
        $url = $request->url();
        $parts = Explode('/', $url);
        $lesson_id = $parts[6];
        $lesson = Lesson::find($lesson_id);
        if (is_null($lesson)) {
            abort(404);
        } else {
            $lesson->delete();
            $course_id = $parts[4];
            $course = Course::find($course_id);
            return redirect('courses/'.$course->id.'/');
        }
    }
    public function update_lesson(Request $request){
        $url = $request->url();
        $parts = Explode('/', $url);
        $lesson_id = $parts[6];
        $course_id = $parts[4];
        $lesson = Lesson::find($lesson_id);
        if (is_null($lesson)) {
            abort(404);
        } else {
            return view('lessons.update', compact('course_id','lesson'));
        }
    }
    public function save(Request $request)
    {
        $lesson = Lesson::find($request['lesson_id']);
        $lesson->description = $request['description'];
        $lesson->save();
        return redirect('/courses/'.$request['course_id'].'/');
    }
}