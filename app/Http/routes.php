<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('pages/landing_page');
});


Route::get('courses','CoursesController@index');
Route::get('courses/create','CoursesController@create');
Route::post('courses','CoursesController@store');
Route::get('courses/delete/{id}','CoursesController@delete');
Route::post('courses/{id}/store-user','CoursesController@store_user');
Route::get('courses/update/{id}','CoursesController@update_course');

Route::get('courses/{id}/add-lesson', 'LessonsController@create');
Route::post('courses/{id}/add-lesson','LessonsController@store_lesson');
Route::get('courses/{id}/delete/{lessNo}','LessonsController@delete');
Route::get('courses/{id}/update/{lessNo}','LessonsController@update_lesson');

Route::post('courses/{id}/quizzes/create','QuizzesController@store');
Route::post('courses/{id}/quizzes/delete/{quizId}','QuizzesController@delete');
Route::get('courses/{id}/quizzes/update/{quizId}','QuizzesController@edit');
Route::post('courses/{id}/quizzes/{quizId}','QuizzesController@update');
Route::get('quiz/{courseId}', 'QuizzesController@takeTest');
Route::post('quiz/{courseId}', 'QuizzesController@grade');
Route::get('studentGrades', 'QuizzesController@getStudentGrades');
Route::get('teacherGrades', 'QuizzesController@getTeacherGrades');

Route::get('courses/{id}/{lessNo}','LessonsController@show_lesson');
Route::post('courses/{id}/{lessNo}','LessonsController@save');
Route::get('courses/{id}','CoursesController@show_course');
Route::post('courses/{id}','CoursesController@save');

Route::get('dashboard','PagesController@dashboard');
Route::get('about','PagesController@about');


Route::resource('user', 'UsersController',
                ['only' => ['edit', 'update']]);


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

Route::get('forum','TopicController@index');
Route::post('forum/delete', 'TopicController@delete');
Route::get('forum/newTopic', 'TopicController@newTopic');
Route::post('forum/newTopic', 'TopicController@createTopic');
Route::get('forum/show/{id}/{pageNo}', 'CommentsController@pages');
Route::get('forum/show/{id}', 'CommentsController@showComments');
Route::post('forum/show/{id}','CommentsController@addComment');
Route::post('comments/delete', 'CommentsController@delete');
Route::get('forum/{pageNo}','TopicController@page');

Route::get('/sudo', 'SudoController@index');
Route::get('sudo/users', 'SudoController@showUsers');
Route::post('sudo/users/delete', 'SudoController@deleteUser');
Route::post('sudo/users/elevate', 'SudoController@elevateUser');
Route::post('sudo/users/downgrade', 'SudoController@downgradeUser');
Route::get('sudo/courses', 'SudoController@showCourses');
Route::post('sudo/courses/delete', 'SudoController@deleteCourse');
Route::get('sudo/topics', 'SudoController@showTopics');
Route::post('sudo/topics/delete', 'SudoController@deleteTopic');
