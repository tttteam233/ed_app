<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 */
class Notification extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'User_id',
        'type'
    ];

    protected $guarded = [];

        
}