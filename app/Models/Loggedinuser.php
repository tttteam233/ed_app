<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Loggedinuser
 */
class Loggedinuser extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'User_id',
        'Room_id'
    ];

    protected $guarded = [];

        
}