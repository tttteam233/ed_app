<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserCanEditCourse
 */
class UserCanEditCourse extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'User_id',
        'Course_id'
    ];

    protected $guarded = [];

        
}