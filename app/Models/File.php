<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class File
 */
class File extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'path',
        'type',
        'added_at',
        'User_id',
        'Document_id',
        'Course_id',
        'Comment_id',
        'Question_id',
        'start_date',
        'end_date',
        'Homework_id',
        'Answer_id'
    ];

    protected $guarded = [];

        
}