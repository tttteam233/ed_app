<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 */
class Log extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'entity_id',
        'entity_name',
        'description'
    ];

    protected $guarded = [];

        
}