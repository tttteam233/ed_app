<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Course
 */
class Course extends Model
{

    protected $table = "course";
    protected $fillable = [
        'title',
        'description',
        'start_date',
        'end_date',
        'department',
        'user_id'
    ];

    protected $guarded = [];

    public function get_photo_path(){
      if ($this->department == "Biology") return "/images/biology.png";
      if ($this->department == "Chemistry") return "/images/chemistry.png";
      if ($this->department == "Computer Science") return "/images/computer_science.png";
      if ($this->department == "Geography") return "/images/geography.png";
      if ($this->department == "Mathematics") return "/images/mathematics.png";
    }
}
