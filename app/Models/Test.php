<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Test
 */
class Test extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'deadline',
        'start_date',
        'end_date',
        'password',
        'type',
        'group_type',
        'last_note',
        'max_note',
        'access_links'
    ];

    protected $guarded = [];

        
}