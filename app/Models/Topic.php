<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = "topics";

    protected $fillable = [
        'title',
        'owner'
    ];

    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }

    public function owner(){
        return User::find($this['owner']);
    }

    public function owner_name(){
      $owner = User::find($this['owner']);
      return $owner['firstName']." ".$owner['lastName'];
    }

}
