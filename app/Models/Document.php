<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Document
 */
class Document extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'start_date',
        'end_date',
        'title',
        'description'
    ];

    protected $guarded = [];

        
}