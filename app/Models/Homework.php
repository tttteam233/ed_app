<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Homework
 */
class Homework extends Model
{


    protected $fillable = [
        'title',
        'description',
        'deadline',
        'points',
        'group_type',
        'type'
    ];

    protected $guarded = [];

        
}