<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';

    protected $fillable = [
        'owner',
        'comment',
        'topic_id'
    ];

    public function owner(){
        return User::find($this['owner']);
    }

    public function topic(){
        return $this->belongsTo('App\Models\Topic');
    }

    public function owner_name(){
      $owner = $this->owner();
      return $owner['firstName']." ".$owner['lastName'];
    }

}
