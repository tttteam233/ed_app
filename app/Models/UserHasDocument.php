<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserHasDocument
 */
class UserHasDocument extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'User_id',
        'Document_id'
    ];

    protected $guarded = [];

        
}