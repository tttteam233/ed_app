<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 */
class Answer extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'type',
        'Homework_id'
    ];

    protected $guarded = [];

        
}