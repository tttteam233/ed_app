<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Lesson
 */
class Lesson extends Model
{


    protected $table = "lesson";
    protected $fillable = [
        'description',
        'course_id'
    ];

    protected $guarded = [];

        
}