<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $table = "quiz";
    protected $fillable = [
        'name',
        'answer1',
        'answer2',
        'answer3',
        'answer4',
        'answer',
        'course_id'
    ];

    protected $guarded = [];

}
