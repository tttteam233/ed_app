<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserHasCourse
 */
class UserHasCourse extends Model
{

    public $timestamps = false;
    protected $table = "user_course_enrolment";
    protected $fillable = [
        'user_id',
        'course_id'
    ];

    protected $guarded = [];

        
}