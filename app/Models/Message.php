<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 */
class Message extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'text',
        'user_id',
        'to_user_id',
        'Room_id'
    ];

    protected $guarded = [];

        
}