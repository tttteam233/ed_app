<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class User
 */
class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    public $timestamps = false;

    protected $fillable = [
        'username',
        'firstName',
        'lastName',
        'email',
        'password',
        'nrmatricol',
        'usergroup_id'
    ];

    protected $hidden = ['password', 'remember_token'];


    protected $table = 'user';

    public function isStudent(){
        return $this->usergroup_id == 1;
    }

    public function isTeacher(){
        return $this->usergroup_id == 2;
    }

    public function isAdmin(){
        return $this->usergroup_id == 3;
    }

    public function get_jpg_photo_path(){
      if ($this->isAdmin()){return "/images/admin.jpg";}
      if ($this->isTeacher()){return "/images/professor.jpg";}
      if ($this->isStudent()){return "/images/student.jpg";}
    }

    public function get_png_photo_path(){
      if ($this->isAdmin()){return "/images/admin.png";}
      if ($this->isTeacher()){return "/images/professor.png";}
      if ($this->isStudent()){return "/images/student.png";}
    }

    public function get_group_name(){
      if ($this->isAdmin()){return "Administrator";}
      if ($this->isTeacher()){return "Professor";}
      if ($this->isStudent()){return "Student";}
    }

    public function get_grade($course_id){
      $grade = Grade::where('user_id','=',$this['id'])->where('course_id','=',$course_id)->first()->grade;
      return $grade;
    }

    public function is_enrolled($course_id){
      $enrolled = $isEnrolled = UserHasCourse::where('course_id', $course_id)->where('user_id','=',$this['id'])->count() == 1;
      return $enrolled;
    }
}
