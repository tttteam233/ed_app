<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserHasTest
 */
class UserHasTest extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'User_id',
        'Test_id'
    ];

    protected $guarded = [];

        
}