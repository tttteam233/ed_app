<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 */
class Question extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'group_type',
        'points',
        'type'
    ];

    protected $guarded = [];

        
}