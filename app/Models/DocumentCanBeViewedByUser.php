<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentCanBeViewedByUser
 */
class DocumentCanBeViewedByUser extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'Document_id',
        'User_id',
        'start_date',
        'end_date',
        'can_be_viewed'
    ];

    protected $guarded = [];

        
}