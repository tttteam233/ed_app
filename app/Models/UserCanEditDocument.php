<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserCanEditDocument
 */
class UserCanEditDocument extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'User_id',
        'Document_id'
    ];

    protected $guarded = [];

        
}