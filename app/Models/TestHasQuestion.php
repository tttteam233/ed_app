<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TestHasQuestion
 */
class TestHasQuestion extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'Test_id',
        'Question_id'
    ];

    protected $guarded = [];

        
}